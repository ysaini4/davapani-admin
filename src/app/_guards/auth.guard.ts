import { Injectable,Inject } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {LOCAL_STORAGE,WebStorageService} from 'angular-webstorage-service';
import {CommonService} from '../_services/common.service'
@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private router: Router,
        @Inject(LOCAL_STORAGE) private storage: WebStorageService,
        private commonService:CommonService
        ) { }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let userid = this.storage.get('userid');
        let token = this.storage.get('token');
        this.commonService.checkValidUser({'userid':userid,'token':token})
        .subscribe(
            res => {
                if(!res.status)
                   this.router.navigate(['/login']);
            },
            err => {
               this.router.navigate(['/login']);
            } 
        );
        return true;
    }
}