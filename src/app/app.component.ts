import { Component,OnInit } from '@angular/core';
import { Router,NavigationEnd} from '@angular/router';
declare var $:any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
	isLoginRoute = false
	constructor(private router: Router) {

	 }
	ngOnInit(){
		/*this.router.events
	    .filter(event => event instanceof NavigationEnd)
	    .subscribe(event => {
	      let currentRouteUrl = this.router.url;
	      if(currentRouteUrl == "/login")
	      	this.isLoginRoute = true
	      else 
	      	this.isLoginRoute = false
	    })*/
	     this.router.events.subscribe(
	     	e => {
		    if (e instanceof NavigationEnd) {
		    let currentRouteUrl = this.router.url;
	     	if(currentRouteUrl == "/login"){
	      		this.isLoginRoute = true
	     	}
	      	else{
	      		this.isLoginRoute = false
	      	} 
		    }
		  }
        )

	}
}
