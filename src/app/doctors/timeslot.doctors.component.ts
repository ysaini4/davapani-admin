import { Component, OnInit } from '@angular/core';
import { UsersService }   from '../_services/users.service';
import { AppointmentsService }   from '../_services/appointments.service';
import { CommonService }   from '../_services/common.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-timeslot-doctors',
  templateUrl: './timeslot.doctors.component.html',
  styleUrls: ['./doctors.component.css']
})
export class TimeSlotDoctorsComponent implements OnInit {
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  appointments = {}
  timeslot = {
    headerRow:[],
    dataRows:[]
  };
  currentPatientDetail = {};
  constructor(private usersService:UsersService,
              private commonService:CommonService,
              private appointmentsService:AppointmentsService,
              private router: Router,
            ) { }
  ngOnInit() {
    this.dropdownList = [
      "aa","dssd"
    ];
    /*this.selectedItems = [
      { item_id: 3, item_text: 'Pune' },
      { item_id: 4, item_text: 'Navsari' }
    ];*/
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 8,
      allowSearchFilter: false
    };
    this.doctorAppointments();
   
  }
  onItemSelect (item:any) {
    console.log(item);
  }
  onSelectAll (items: any) {
    console.log(items);
  }
  
   public doctorAppointments():void{
    var data = {
        "doctor_id":"String"
    }
    this.appointmentsService.doctorAppointments(data)
            .subscribe(
                res => {
                  let app_res = res.data;
                  let app_ln = Object.keys(res.data).length;
                  for(let i=0;i<app_ln;i++){
                    if(app_res[i].book_time){
                      if(this.appointments[app_res[i].book_time]){
                        this.appointments[app_res[i].book_time].push(app_res[i])
                      } else{
                        this.appointments[app_res[i].book_time] = Array(app_res[i]);
                      }
                    }
                  }
                  this.timeslotManagment();
                  if(res.status){

                  }
                },
                err => {
                    this.commonService.showNotification(err.message,'danger')
                } 
            );      
  }

  public timeslotManagment():void{
    var today = new Date();
    var headerrow = Array('Time');
    for(var i=0;i<7;i++){
        var nextday = new Date();
        nextday.setDate(today.getDate()+i);
        headerrow.push(this.getDayByDate(nextday))
    }
    this.timeslot.headerRow = headerrow
    var datarows = Array();
    //var hr = today.getHours();
    for(var i=0;i<48;i++){
      var nexttime = new Date();
      nexttime.setTime(today.getTime() + (i*60*30*1000)); 
      var timeHR,timeMIN;
      var hr = nexttime.getHours();
      var min = nexttime.getMinutes();
      if(min>=30){
        timeMIN = '30';
      } else {
        timeMIN = '00';
      }

      var mod;
      if(hr>12){
        timeHR = (nexttime.getHours())%12;
        mod = 'PM'
      }else if(hr==0){
        timeHR = 12;
        mod = 'AM'
      }
      else {
        timeHR = (nexttime.getHours());
        mod = 'AM'
        if(timeHR == 12)
          mod = 'PM'


      }
      var row = [];
      var c_time = timeHR+':'+timeMIN+' '+mod;
      row['time'] = c_time;
       for(var j=0;j<7;j++){
        var nextday = new Date();
        nextday.setHours(hr);
        nextday.setMinutes(timeMIN);
        nextday.setSeconds(0);
        nextday.setMilliseconds(0);
        //nextday.setTime(today.getTime() + (j*60*30*1000));
        nextday.setDate(today.getDate()+j);
        let time = nextday.getTime()/1000;
        if(this.appointments[time]){
          row[j] = this.appointments[time][0];
          row[j].book_time = this.getDayByDate(nextday)+' '+c_time;
        }else {
          row[j] = time
        }
        }
      datarows.push(row)
     // datarow.push({time:time+':30 '+mod})
    }
    this.timeslot.dataRows = datarows;

  }
  public setCurrentSlotPopup(data):void{
    this.currentPatientDetail = data;
  }
  public getDayByDate(date):string{
    var weekday = new Array(7);
    weekday[0] =  "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    var month = new Array();
    month[0] = "Jan";
    month[1] = "Feb";
    month[2] = "Mar";
    month[3] = "Apr";
    month[4] = "May";
    month[5] = "Jun";
    month[6] = "Jul";
    month[7] = "Aug";
    month[8] = "Sep";
    month[9] = "Oct";
    month[10] = "Nov";
    month[11] = "Dec";
    return weekday[date.getDay()]+ ' ('+date.getDate()+' '+month[date.getMonth()]+')'
  }
}
