import { Component, OnInit ,Inject} from '@angular/core';
import { UsersService }   from '../_services/users.service';
import {LOCAL_STORAGE,WebStorageService} from 'angular-webstorage-service';

@Component({
  selector: 'app-doctors',
  templateUrl: './doctors.component.html',
  styleUrls: ['./doctors.component.css'],
 
})
export class DoctorsComponent implements OnInit {
	
  public doctors;
  constructor(private usersService:UsersService,
               @Inject(LOCAL_STORAGE) private storage: WebStorageService,
    ) { }
  ngOnInit() {
  	this.doctors = {
    	headerRow: [ 'Name', 'UserName', 'Type', 'Mobile','Action  ']
    }
    this.hospitalList()
  }

    saveDoctorListData(doctorId):void{
     var SaveDrLIstdata;
      for(var i=0;i<this.doctors.dataRows.length;i++){
        if(this.doctors.dataRows[i]._id == doctorId){
          SaveDrLIstdata = this.doctors.dataRows[i];
        }
      }
       this.storage.set('selected-doctor-detail', SaveDrLIstdata);
      
    }

  hospitalList():void{
  this.usersService.doctorList()
      .subscribe(
        res => {
          this.doctors.dataRows = res.data;

    		},
    		err => {
    			console.log(err.error,'error')
    		} 
    	);  		
  }
}
