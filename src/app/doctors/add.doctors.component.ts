import { Component, OnInit } from '@angular/core';
import { UsersService }   from '../_services/users.service';
import { CommonService }   from '../_services/common.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-add-doctors',
  templateUrl: './add.doctors.component.html',
  styleUrls: ['./doctors.component.css']
})
export class AddDoctorsComponent implements OnInit {
   dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  editorConfig = {
    editable: true,
    spellcheck: false,
    height: '10rem',
    minHeight: '15rem',
    placeholder: 'Type something. Test the Editor... ヽ(^。^)丿',
    translate: 'no'
  };
  constructor(private usersService:UsersService,
              private commonService:CommonService,
              private router: Router,
            ) { }
  ngOnInit() {
    this.usersService.doctorSpeciality().subscribe(
                res => {
                  this.dropdownList = res.data;              
                },
               
            );      
   
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 8,
      allowSearchFilter: false
    };
 
  }
  onItemSelect (item:any) {
    console.log(item);
  }
  onSelectAll (items: any) {
    console.log(items);
  }
  
   public add(input):void{  
    var data = {
        "name":{
          "first":input.first,
          "last": input.last,
        },
        "username":input.username,
        "password":input.password,
        "mobile":input.mobile,
        "speciality":input.speciality,
        "type":"doctor",
        "city":input.city,
        "state":input.state,
        "email":input.email,        
        "address_line":input.address_line,
        "zip_code":input.postalcode,
        "about":input.about,
        "country":input.country
    }
    
    this.usersService.addUser(data)
            .subscribe(
                res => {
                    if(res.status){
                      this.router.navigate(['/doctors']);
                      this.commonService.showNotification(res.message)
                    }
                },
                err => {
                    this.commonService.showNotification(err.message,'danger')
                } 
            );      
  }
}
