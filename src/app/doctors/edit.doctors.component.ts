import { Component, OnInit,NgModule ,Inject} from '@angular/core';
import { UsersService }   from '../_services/users.service';
import { CommonService }   from '../_services/common.service';
import { Router} from '@angular/router';
import { DoctorsComponent } from './doctors.component';
import {LOCAL_STORAGE,WebStorageService} from 'angular-webstorage-service';

@Component({
  selector: 'app-edit-doctors',
  templateUrl: './edit.doctors.component.html',
  styleUrls: ['./doctors.component.css']
})

export class EditDoctorsComponent implements OnInit {
  dropdownSettings = {}; 
  GetDoctorList:any;
  dropdownList=[];

  constructor(private usersService:UsersService,
              private commonService:CommonService,
              private router: Router,
              private doctorsComponent  : DoctorsComponent,
               @Inject(LOCAL_STORAGE) private storage: WebStorageService,
            ) {
          this.GetDoctorList = this.storage.get('selected-doctor-detail');
  }
  ngOnInit() {
    this.usersService.doctorSpeciality().subscribe(
                res => {
                  this.dropdownList = res.data;              
                },
               
            );   

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 8,
      allowSearchFilter: false
    };
  }

  
    onItemSelect (item:any) {
    console.log(item);
  }
  onSelectAll (items: any) {
    console.log(items);
  }

   public edit(input,id):void{
      
        var data = {
          "_id":id,
          "update_data":{
              "name":{
              "first":input.first,
              "last": input.last
            },
            "username":input.username,
            "password":input.password,
            "mobile":input.mobile,
            "speciality":input.speciality,
            "type":"doctor",
            "city":input.city,
            "state":input.state,
            "email":input.email,        
            "address_line":input.address_line,
            "zip_code":input.postalcode,
            "about":input.about,
            "country":input.country
              }
          }


    this.usersService.editdoctor(data)
            .subscribe(
                res => {
                    if(res.status){
                      this.router.navigate(['/doctors']);
                      this.commonService.showNotification(res.message)
                    }
                },
                err => {
                    this.commonService.showNotification(err.message,'danger')
                } 
            );      
  }
}

