import { RouterModule,Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { DashboardComponent }   from './dashboard/dashboard.component';
import { LoginComponent }   from './login/login.component';
import { UserComponent }   from './user/user.component';
import { TableComponent }   from './table/table.component';
import { TypographyComponent }   from './typography/typography.component';
import { IconsComponent }   from './icons/icons.component';
import { HospitalsComponent } from './hospitals/hospitals.component';
import { AddHospitalsComponent } from './hospitals/add.hospitals.component';
import { DoctorsComponent } from './doctors/doctors.component';
import { AddDoctorsComponent } from './doctors/add.doctors.component';
import { TimeSlotDoctorsComponent } from './doctors/timeslot.doctors.component';
import { EditDoctorsComponent } from './doctors/edit.doctors.component';
//import { MapsComponent }   from './maps/maps.component';
import { NotificationsComponent }   from './notifications/notifications.component';
import { UpgradeComponent }   from './upgrade/upgrade.component';
import { LabsComponent } from './labs/labs.component';
import { AddLabsComponent } from './labs/add.labs.component';
import { EditLabsComponent } from './labs/edit.labs.components';
import { AuthGuard }   from './_guards';
import { LabTestsComponent } from './lab-tests/lab-tests.component';
import { AddLabTestsComponent } from './lab-tests/add-lab-tests.component';
import { EditLabTestsComponent } from './lab-tests/edit-lab-tests.component';




export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'user',
        pathMatch: 'full',

    },
    {
        path: 'login',
        component: LoginComponent
        
    },
    {
        path: 'hospitals',
        children: [                          //<---- child components declared here
            {
                path:'',
                component: HospitalsComponent
            },
            {
                path:'add',
                component: AddHospitalsComponent
            }
            
        ]
    },
    {
        path: 'doctors',
        children: [                          //<---- child components declared here
            {
                path:'',
                component: DoctorsComponent
            },
            {
                path:'add',
                component: AddDoctorsComponent
            },
            {
                path:'timeslot',
                component: TimeSlotDoctorsComponent
            },
            {
                path:'edit/:id',
                component: EditDoctorsComponent
            }
        ]
    },
     {
        path: 'labs',
        children: [                          //<---- child components declared here
            {
                path:'',
                component: LabsComponent
            },
            {
                path:'add',
                component: AddLabsComponent
            },
            {
                path:'edit/:id',
                component: EditLabsComponent
            }

            
        ]
    },
    {
        path: 'labtest',
        children: [                          //<---- child components declared here
            {
                path:'',
                component: LabTestsComponent
            },
            {
                path:'add',
                component: AddLabTestsComponent
            },
            {
                path:'edit/:id',
                component: EditLabTestsComponent
            }

            
        ]
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
       /* canActivate: [AuthGuard]*/
    },

    {
        path: 'user',
        component: UserComponent,
    },
    {
        path: 'table',
        component: TableComponent,
    },
    {
        path: 'typography',
        component: TypographyComponent,
    },
    {
        path: 'icons',
        component: IconsComponent,
    },
    {
        path: 'maps',
        component: IconsComponent,
    },
    {
        path: 'notifications',
        component: NotificationsComponent,
    },
    {
        path: 'upgrade',
        component: UpgradeComponent,
    }
];

