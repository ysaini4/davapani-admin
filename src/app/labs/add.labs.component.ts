import { Component, OnInit } from '@angular/core';
import { UsersService }   from '../_services/users.service';
import { CommonService }   from '../_services/common.service';
import { Router} from '@angular/router';


@Component({
  selector: 'app-add-labs',
  templateUrl: './add.labs.component.html',
  styleUrls: ['./labs.component.css']
})
export class AddLabsComponent implements OnInit {
	public labs;
  constructor(private usersService:UsersService,
  			      private commonService:CommonService,
              private router: Router,
  	) { }

  ngOnInit() {  	
 
  }
     public add(input):void{  
    var data = {
       	"name":input.name,
       	"title":input.title,
        "about":input.about
        
    }
    
    this.usersService.addLabs(data)
            .subscribe(
                res => {
                    if(res.status){
                      this.router.navigate(['/labs']);
                      this.commonService.showNotification(res.message)
                    }
                },
                err => {
                    this.commonService.showNotification(err.message,'danger')
                } 
            );      
  }
}
