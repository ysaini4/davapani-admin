import { Component, OnInit,NgModule ,Inject} from '@angular/core';
import { UsersService }   from '../_services/users.service';
import { CommonService }   from '../_services/common.service';
import { Router} from '@angular/router';
import {LOCAL_STORAGE,WebStorageService} from 'angular-webstorage-service';


@Component({
  selector: 'app-edit-labs',
  templateUrl: './edit.labs.components.html',
  styleUrls: ['./labs.component.css']
})
export class EditLabsComponent implements OnInit {
	public labs;
  GetLabsList;
  constructor(private usersService:UsersService,
  			     private commonService:CommonService,
             private router: Router,
             @Inject(LOCAL_STORAGE) private storage: WebStorageService,
  	) { 
        this.GetLabsList = this.storage.get('selected-labs-detail');
  }

  ngOnInit() {  	
 
  }
    public edit(input,id):void{  

    var data = {
        "_id":id,
          "update_data":{
       	"name":input.name,
       	"title":input.title,
        "about":input.about
      }  
    }
    
    this.usersService.UpdateLabs(data)
            .subscribe(
                res => {
                    if(res.status){
                      this.router.navigate(['/labs']);
                      this.commonService.showNotification(res.data)
                    }
                },
                err => {
                    this.commonService.showNotification(err.data,'danger')
                } 
            );      
  }
}
