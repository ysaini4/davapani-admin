import { Component, OnInit,Inject } from '@angular/core';
import { UsersService }   from '../_services/users.service';
import { CommonService }   from '../_services/common.service';
import { Router} from '@angular/router';
import {LOCAL_STORAGE,WebStorageService} from 'angular-webstorage-service';


@Component({
  selector: 'app-labs',
  templateUrl: './labs.component.html',
  styleUrls: ['./labs.component.css']
})
export class LabsComponent implements OnInit {
	public labs;
  constructor(private usersService:UsersService,
               @Inject(LOCAL_STORAGE) private storage: WebStorageService,
    ) { }

  ngOnInit() {
  	this.labs = {
    	headerRow: [ 'Name','Title', 'About','Action  ']
    }
  

  this.usersService.LabsList().subscribe(
        res => {
          
          this.labs.dataRows = res.data;
          /*console.log(this.labs.dataRows,'2020');*/

    		},
    		err => {
    			//console.log(err.error,'error')
    		} 
    	);  
}

saveLabsListData(id){
 var  savelabspecificdata ;  
  for (var i =0; i<this.labs.dataRows.length;i++) {     
     if(this.labs.dataRows[i]._id == id){
      savelabspecificdata =this.labs.dataRows[i]
     }
      this.storage.set('selected-labs-detail', savelabspecificdata);
  }
}
}
