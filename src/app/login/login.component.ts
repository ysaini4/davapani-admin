import { Component, OnInit,Inject, Injectable } from '@angular/core';
import { UsersService }   from '../_services/users.service';
import {Router} from "@angular/router";
import {LOCAL_STORAGE,WebStorageService} from 'angular-webstorage-service';
@Injectable()
@Component({
    selector: 'login-cmp',
    templateUrl: 'login.component.html',
    styleUrls: [ 'login.component.css' ]
})
export class LoginComponent implements OnInit {
	ngOnInit(){

        
        
	}
	constructor(
		private usersService:UsersService,
		private router: Router,
		@Inject(LOCAL_STORAGE) private storage: WebStorageService
		) { }
  	save(frmValues):void{
    frmValues.type = "admin"
	this.usersService.getLogin(frmValues)
    	.subscribe(
    		res => {
				this.storage.set('token', res.data.token);
				this.storage.set('userid', res.data.userid);
				this.router.navigate(['/hospitals']);
    		},
    		err => {
    			console.log(err.error,'error')
    		} 
    	);  		
  	}
}