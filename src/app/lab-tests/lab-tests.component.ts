import { Component, OnInit ,Inject} from '@angular/core';
import { UsersService }   from '../_services/users.service';
import { CommonService }   from '../_services/common.service';
import { Router} from '@angular/router';
import {LOCAL_STORAGE,WebStorageService} from 'angular-webstorage-service';

@Component({
  selector: 'app-lab-tests',
  templateUrl: './lab-tests.component.html',
  styleUrls: ['./lab-tests.component.css']
})
export class LabTestsComponent implements OnInit {
  public labsTest
  savelabTestspecificdata;
  constructor(private usersService:UsersService,
               @Inject(LOCAL_STORAGE) private storage: WebStorageService) { }

  ngOnInit() {
  	this.labsTest = {
    	headerRow: ['Name','Title','About','Action']
    }
  

  this.usersService.LabsTestList().subscribe(
        res => {
          
          this.labsTest.dataRows = res.data;
          console.log(this.labsTest.dataRows,'this.labsTest.dataRows');
          },
    		err => {
    	
    		} 
    	);  
  }

  saveLabsTestListData(id){
	  for (var i =0; i<this.labsTest.dataRows.length;i++) {     
	     if(this.labsTest.dataRows[i]._id == id){
	      this.savelabTestspecificdata = this.labsTest.dataRows[i];
	     }
	      this.storage.set('selected-labs-test-detail', this.savelabTestspecificdata);
	  }
	}

}
