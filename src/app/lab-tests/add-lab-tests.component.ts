import { Component, OnInit ,Inject} from '@angular/core';
import { UsersService }   from '../_services/users.service';
import { CommonService }   from '../_services/common.service';
import { Router} from '@angular/router';
import {LOCAL_STORAGE,WebStorageService} from 'angular-webstorage-service';

@Component({
  selector: 'app-add-lab-tests',
  templateUrl: './add-lab-tests.component.html',
  styleUrls: ['./lab-tests.component.css']
})
export class AddLabTestsComponent implements OnInit {
	
  constructor(private usersService:UsersService,
               @Inject(LOCAL_STORAGE) private storage: WebStorageService,
               private router: Router,
                private commonService:CommonService) { }

  ngOnInit() { }
  

 public add(input):void{  
    var data = {
       	"name":input.name,
       	"title":input.title,
        "about":input.about
        
    }
    
    this.usersService.addLabsTest(data)
            .subscribe(
                res => {
                    if(res.status){
                      this.router.navigate(['/labtest']);
                      this.commonService.showNotification(res.message)
                    }
                },
                err => {
                    this.commonService.showNotification(err.data,'danger')
                } 
            );      
  }
  }

