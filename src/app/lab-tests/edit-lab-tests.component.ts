import { Component, OnInit ,Inject} from '@angular/core';
import { UsersService }   from '../_services/users.service';
import { CommonService }   from '../_services/common.service';
import { Router} from '@angular/router';
import {LOCAL_STORAGE,WebStorageService} from 'angular-webstorage-service';

@Component({
  selector: 'app-edit-lab-tests',
  templateUrl: './edit-lab-tests.component.html',
  styleUrls: ['./lab-tests.component.css']
})
export class EditLabTestsComponent implements OnInit {
  public GetLabTestsList;

   constructor(private usersService:UsersService,
             private commonService:CommonService,
             private router: Router,
             @Inject(LOCAL_STORAGE) private storage: WebStorageService,
    ) { 
        this.GetLabTestsList = this.storage.get('selected-labs-test-detail');
        console.log(this.GetLabTestsList,'54545');
  }

  ngOnInit() { }

   public edit(input,id):void{  

    var data = {
        "_id":id,
          "update_data":{
        "name":input.name,
        "title":input.title,
        "about":input.about
      }  
    }
    
    this.usersService.UpdateLabsTest(data)
            .subscribe(
                res => {
                    if(res.status){
                      this.router.navigate(['/labtest']);
                      this.commonService.showNotification(res.data)
                    }
                },
                err => {
                    this.commonService.showNotification(err.data,'danger')
                } 
            );      
  }

}
