import { Component, OnInit } from '@angular/core';
import { UsersService }   from '../_services/users.service';
import { CommonService }   from '../_services/common.service';

import { Router} from '@angular/router';


@Component({
  selector: 'app-add-hospitals',
  templateUrl: './add.hospitals.component.html',
  styleUrls: ['./hospitals.component.css']
})
export class AddHospitalsComponent implements OnInit {
    constructor(
        private usersService:UsersService,
		private commonService:CommonService,
        private router: Router,
		) { }
    ngOnInit(){
   }
    public add(input):void{
        var data = {
            "name":input.hospitalname,
            "username":input.username,
            "password":input.password,
            "mobile":input.mobile,
            "type":"hospital",
            "city":input.city,
            "state":input.state,
            "country":input.country
        }
    this.usersService.addUser(data)
            .subscribe(
                res => {
                    if(res.status){
                      this.router.navigate(['/hospitals']);
                      this.commonService.showNotification(res.message)
                    }
                },
                err => {
                    this.commonService.showNotification(err.error,'danger')
                    console.log(err.error,'error')
                } 
            );      
        }

}
