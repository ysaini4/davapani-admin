import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {PopoverModule} from "ngx-popover";
import { NgxEditorModule } from 'ngx-editor';

import { AppComponent } from './app.component';
import { AppRoutes } from './app.routing';
import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';
import { FixedPluginModule} from './shared/fixedplugin/fixedplugin.module';
//import { NguiMapModule} from '@ngui/map';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { LoginComponent }   from './login/login.component';
import { UserComponent }   from './user/user.component';
import { TableComponent }   from './table/table.component';
import { TypographyComponent }   from './typography/typography.component';
import { IconsComponent }   from './icons/icons.component';
//import { MapsComponent }   from './maps/maps.component';
import { NotificationsComponent }   from './notifications/notifications.component';
import { UpgradeComponent }   from './upgrade/upgrade.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { StorageServiceModule} from 'angular-webstorage-service';

import { AuthGuard }   from './_guards';
import { UsersService }   from './_services/users.service';
import { CommonService }   from './_services/common.service';
import { AppointmentsService }   from './_services/appointments.service';

import { HospitalsComponent } from './hospitals/hospitals.component';
import { DoctorsComponent } from './doctors/doctors.component';
import { AddDoctorsComponent } from './doctors/add.doctors.component';
import { AddHospitalsComponent } from './hospitals/add.hospitals.component';
import { TimeSlotDoctorsComponent } from './doctors/timeslot.doctors.component';
import {EditDoctorsComponent} from './doctors/edit.doctors.component';
import { LabsComponent } from './labs/labs.component';
import { AddLabsComponent } from './labs/add.labs.component';
import { EditLabsComponent } from './labs/edit.labs.components';
import { LabTestsComponent } from './lab-tests/lab-tests.component';
import { AddLabTestsComponent } from './lab-tests/add-lab-tests.component';
import { EditLabTestsComponent } from './lab-tests/edit-lab-tests.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    UserComponent,
    TableComponent,
    TypographyComponent,
    IconsComponent,
//    MapsComponent,
    NotificationsComponent,
    UpgradeComponent,
    HospitalsComponent,
    DoctorsComponent,
    AddDoctorsComponent,
    AddHospitalsComponent,
    TimeSlotDoctorsComponent,
    EditDoctorsComponent,
    LabsComponent,
    LabTestsComponent,
    AddLabsComponent,
    EditLabsComponent,
    AddLabTestsComponent,
    EditLabTestsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(AppRoutes),
    SidebarModule,
    NavbarModule,
    FooterModule,
    FixedPluginModule,
    HttpClientModule,
    FormsModule,
    StorageServiceModule,
    NgMultiSelectDropDownModule.forRoot(),
    PopoverModule,
    NgxEditorModule
    //NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=YOUR_KEY_HERE'})

  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    AuthGuard,
    UsersService,
    CommonService,
    AppointmentsService,
    DoctorsComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
