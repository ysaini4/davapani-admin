import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
declare var $:any;

@Injectable()
export class CommonService {
	serverUrl = environment.serverUrl
  	constructor(private http: HttpClient) { }
    showNotification(msg,type='success'){
        //var type = ['','info','success','warning','danger'];
        var icon = {'info':'ti-info','success':'ti-thumb-up','warning':'ti-eye','danger':'ti-thumb-down'}
        //var color = Math.floor((Math.random() * 4) + 1);
    	$.notify({
        	icon: icon[type],
        	message: msg
        },{
            type: type,
            timer: 4000,
            placement: {
                from: 'bottom',
                align: 'right'
            }
        });
    }
  	checkValidUser(inputs):Observable<checkValidUser>{
  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json',
	    	'token':inputs.token,
	    	'userid':inputs.userid
	  	})
	};
  		let url = this.serverUrl+"checkvaliduser";
  		return this.http.get<checkValidUser>(url,httpOptions);
	}
}

interface checkValidUser {
	status:boolean,
	app:string,
	message:string
}
