import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class AppointmentsService {
	serverUrl = environment.serverUrl

  	constructor(private http: HttpClient) { }
	doctorAppointments(inputs):Observable<doctor_appointment> {
	  	let httpOptions = {
		  	headers: new HttpHeaders({
		    	'Content-Type':  'application/json'
	  		})
		};
		let data = {
			"data":inputs	
		}
  		let url = this.serverUrl+"get_appointment";
  		return this.http.post<doctor_appointment>(url,data,httpOptions);
	}
}
interface doctor_appointment  {
	status:boolean,
	app:string,
	auth:string,
	data:{
		patient_name:string,
		patient_mobile:string,
		doctor_id:string,
		doctor_name:string,
		book_time:string
	}
}