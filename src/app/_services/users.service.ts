import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable()
export class UsersService {
	serverUrl = environment.serverUrl
  	constructor(private http: HttpClient) { }
  	getLogin(inputs):Observable<getLogin> {
  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json'
	  	})
		};
  		let url = this.serverUrl+"login";
  		let data = {
			"data":inputs
		}
  		return this.http.post<getLogin>(url,data,httpOptions);
	}
	hospitalList():Observable<hospitalList> {
  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json'
	  	})
		};
  		let url = this.serverUrl+"hospital_list";
  		return this.http.get<hospitalList>(url,httpOptions);
	}
	doctorList():Observable<doctorList> {
  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json'
	  	})
		};
  		let url = this.serverUrl+"doctor_list";
  		return this.http.get<doctorList>(url,httpOptions);
	}
	addUser(data):Observable<addUser> {
		var body = {
			data:data
		}
  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json'
	  	})
		};
  		let url = this.serverUrl+"adduser";
  		return this.http.post<addUser>(url,body,httpOptions);
	}

	editdoctor(data):Observable<editDoctor> {
		var body = {
			data:data
		}
  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json'
	  	})
		};
  		let url = this.serverUrl+"updatedoctor";
  		return this.http.post<editDoctor>(url,body,httpOptions);
	}


	//for doctor speciality

	doctorSpeciality():Observable<speciality>{
  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json'
	  	})
		};
  		let url = this.serverUrl+"doctor_speciality";
  		return this.http.get<speciality>(url,httpOptions);

	}

	//for doctor edit..
	editUser(data):Observable<editUser> {
		var body = {
			data:data
		}
  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json'
	  	})
		};
  		let url = this.serverUrl+"editUser";
  		return this.http.post<editUser>(url,body,httpOptions);
	}
	//getting labs listing
	LabsList():Observable<labList> {

  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json'
	  	})
		};
  		let url = this.serverUrl+"lablist";
  		return this.http.get<labList>(url,httpOptions);
	}

	addLabs(data):Observable<addLabs> {
		var body = {
			data:data
		}
  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json'
	  	})
		};
  		let url = this.serverUrl+"addlab";
  		return this.http.post<addLabs>(url,body,httpOptions);
	}
	UpdateLabs(data):Observable<updateLabs> {
		console.log(data,'3030');
		var body = {
			data:data
		}
  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json'
	  	})
		};
  		let url = this.serverUrl+"updatelab";
  		return this.http.post<updateLabs>(url,body,httpOptions);
	}
	LabsTestList():Observable<labTestList> {

  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json'
	  	})
		};
  		let url = this.serverUrl+"labtestlist";
  		return this.http.get<labTestList>(url,httpOptions);
	}
	
	addLabsTest(data):Observable<addlabstest> {
		var body = {
			data:data
		}
  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json'
	  	})
		};
  		let url = this.serverUrl+"addlabtest";
  		return this.http.post<addlabstest>(url,body,httpOptions);
	}
	UpdateLabsTest(data):Observable<updateLabsTest> {
			var body = {
			data:data
		}
  	let httpOptions = {
	  	headers: new HttpHeaders({
	    	'Content-Type':  'application/json'
	  	})
		};
  		let url = this.serverUrl+"updatelabtest";
  		return this.http.post<updateLabsTest>(url,body,httpOptions);
	}

}
interface updateLabsTest {
	status:boolean,
	data:string,
}
interface labTestList{
	status:boolean,
	app:string,
	auth:string,
	data:[{name:string,title:string,about:string}]
}
interface updateLabs{
	status:boolean,
	data:string,

}
interface addlabstest{
	status:boolean,
	message:string
}
interface labList{
	status:boolean,
	app:string,
	auth:string,
	data:[{name:string,title:string,about:string}]
}
interface addLabs{
	status:boolean,
	message:string
}
interface getLogin  {
	status:boolean,
	app:string,
	auth:string,
	data:{token:string,userid:string},
}
interface hospitalList  {
	status:boolean,
	app:string,
	auth:string,
	data:[{username:string,name:string,mobile:string,type:string}],
}
interface doctorList  {
	status:boolean,
	app:string,
	auth:string,
	data:[{username:string,name:string,mobile:string,type:string}],
}
interface addUser  {
	status:boolean,
	message:string
}
interface editDoctor  {
	status:boolean,
	message:string
}
interface editUser {
	status:boolean,
	message:string	
}
interface speciality {
	status:boolean,
	data:[string]		
}